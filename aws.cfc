component accessors=true {

	property name='regions' type='com.amazonaws.regions.Regions' getter=false setter=false;

	public aws function init() {
		variables.regions = CreateAWSObject( 'regions.Regions' );
		return this;
	}

	private function CreateAWSObject(required string name) {
		return CreateObject(
			'java',
			'com.amazonaws.' & arguments.name,
			[
				'aws-java-sdk-1.11.103/aspectjrt-1.8.2.jar',
				'aws-java-sdk-1.11.103/aspectjweaver.jar',
				'aws-java-sdk-1.11.103/aws-java-sdk-1.11.103.jar',
				'aws-java-sdk-1.11.103/commons-codec-1.9.jar',
				'aws-java-sdk-1.11.103/commons-logging-1.1.3.jar',
				'aws-java-sdk-1.11.103/freemarker-2.3.9.jar',
				'aws-java-sdk-1.11.103/httpclient-4.5.2.jar',
				'aws-java-sdk-1.11.103/httpcore-4.4.4.jar',
				'aws-java-sdk-1.11.103/ion-java-1.0.2.jar',
				'aws-java-sdk-1.11.103/jackson-annotations-2.6.0.jar',
				'aws-java-sdk-1.11.103/jackson-core-2.6.6.jar',
				'aws-java-sdk-1.11.103/jackson-databind-2.6.6.jar',
				'aws-java-sdk-1.11.103/jackson-dataformat-cbor-2.6.6.jar',
				'aws-java-sdk-1.11.103/javax.mail-api-1.4.6.jar',
				'aws-java-sdk-1.11.103/jmespath-java-1.11.103.jar',
				'aws-java-sdk-1.11.103/joda-time-2.8.1.jar',
				'aws-java-sdk-1.11.103/json-path-2.2.0.jar',
				'aws-java-sdk-1.11.103/slf4j-api-1.7.16.jar',
				'aws-java-sdk-1.11.103/spring-beans-3.0.7.RELEASE.jar',
				'aws-java-sdk-1.11.103/spring-context-3.0.7.RELEASE.jar',
				'aws-java-sdk-1.11.103/spring-core-3.0.7.RELEASE.jar',
				'aws-java-sdk-1.11.103/spring-test-3.0.7.RELEASE.jar'
			]
		);
	}

	private function getRegions() {
		return variables.regions;
	}

	public function getRegion(
		string region = 'eu-west-1'
	) {
		return getRegions().fromName(
			arguments.region
		);
	}

	private any function getMyClient() {
		return variables.myClient;
	}

	public function setRegion(string region='us-east-1') {
		getMyClient().configureRegion( getRegion(arguments.region) );

		return this;
	}

}