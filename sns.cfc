component accessors=true extends='aws' {

	property name='myClient' type='com.amazonaws.services.sns.AmazonSNSClient' getter=false setter=false;		

	public sns function init()
	{

		super.init(argumentCollection = arguments);

		variables.myClient = CreateAWSObject( 'services.sns.AmazonSNSClientBuilder' ).defaultClient();

		return this;
	}

	public any function publish(required string arnTopic, required string message)
	{
		return getMyClient().publish(arguments.arnTopic, arguments.message);
	}
}